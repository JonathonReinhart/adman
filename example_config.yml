# Any path entries can be given as either absolute paths
# or as relative paths, relative to the config file directory.

# The DNS name of the domain
domain: ad.example.com

# LDAP authentication
ldap_auth:
  # Mode of authentication; options: gssapi
  mode: gssapi

  # gssapi options
  # These are required if adman is to automatically manage kerberos tickets.
  # These can be left unset if adman is to use the current user's ticket.

  # Kerberos username
  krb_username: domain-janitor

  # Kerberos keytab path
  krb_keytab: domain-janitor.keytab

  # Kerberos credential cache path
  krb_cache: /tmp/domain-janitor.cc


# Path to file to which changes are logged
# Default: write to stderr
changelog: /var/log/adman-changes.log


# Assign RFC2307 uidNumber/gidNumber attributes to users and groups
id_assign:
  # Range of values to use for assigning uidNumber attributes
  uid_range:
    min: 100000
    max: 200000

  # Range of values to use for assigning gidNumber attributes
  gid_range:
    min: 100000
    max: 200000

  # Assign uidNumber to computer accounts? (default True)
  computers: True

  # The "only" key, if present, will restrict ID assignment to members of the
  # given containers. Optional scope can be be 'one' or 'subtree' (default).
  # This applies to both users (including computers) and groups.
  only:
    # Recommended to always include these three containers
    CN=Users:
    CN=Computers:
    OU=Domain Controllers:

    # Other custom containers
    OU=ADTest People:
      scope: one
# end id_assign

# Automatically create user directories
userdirs:
    # basepath is the directory in which to create each userdir
  - basepath: '//dc1.ad-test.vx/netlogon/users/'
    # Limit to these users
    only:
      OU=ADTest People:
        scope: one
    # owner is the account name to set as the owner of each userdir
    owner: Fileshare Owner
    group: Storage Admins
    acl:
      - "${user}:0/0/0x001201ff"    # Basically everything but delete
      - "${user}:0/11/0x001f01ff"   # Everything (inherit only)
      - "Domain Users:0/0/0x001200a9"   # Users can... traverse? (Requires access-based enumeration)
    # additional subdirectories to create in each user's directory
    # owner and group are inherited from above
    subdirs:
      - name: 'public'
        acl:
          - "${user}:0/0/0x001f01ff"        # Everything
          - "Domain Users:0/0/0x001200a9"   # TODO
# end userdirs


# Apply consistent UPN suffixes to all members of a container (OU)
upn_suffixes:

  # The key is the container which specifies the set of users to which the UPN
  # suffix will be applied. There are two ways to specify the UPN suffix to be
  # applied to a container:

  # 1. The simple format just specifies the suffix:
  CN=Users: example.com

  # 2. The complex format allows the scope to be specified,
  # which can be either 'one' or 'subtree' (the default)
  OU=Special Users,OU=People:
    suffix: special.com
    scope: one
# end upn_suffixes


# Notify users when their password is about to expire
# (Useful for LDAP-only users)
password_expiry_notification:
  # Users should be notified each time their password expires
  # in this many days
  days: [7, 3, 2, 1, 0]


  # The template to use for sending mail
  template_file: example_pwnotify.tmpl
# end password_expiry_notification


# Settings used for sending email
smtp:
  # The email address from which messages should be sent (required)
  email_from: "Domain Janitor <domain-janitor@example.com>"

  # Host is optional; defaults to localhost
  host: "smtp.example.com"

  # Port is optional; defaults to 25 (or 465 for SSL)
  port: 25

  # Username/password are optional
  username: "joe"
  password: "password"

  # Encryption is optional and can be "starttls" or "ssl"
  encryption: "starttls"


# Find stale user/computer accounts that haven't recently been logged into
stale_accounts:
  # Admin email to which reports are sent
  email_to: "System Administrator <sysadmin@example.com>"

  # Domain-wide settings
  # How old an account must be before it is "stale"
  older_than: "120 days"

  # Whether or not to disable stale accounts
  #disable: True

  # Whether or not to report already-disabled accounts (default: False)
  include_disabled: True

  # By default, the entire domain is searched for stale user and computer
  # accounts. That can be overridden for user and computer accounts separately.
  # LDAP containers to be searched can be specified here, along with settings
  # which override those above.
  users:
    OU=Special Users,OU=People:
      # LDAP search scope; can be 'one' or 'subtree' (default).
      scope: one
      older_than: "30 days"
      disable: True
      include_disabled: False

    CN=Users:
      #scope: subtree

  computers:
    CN=Computers:
      disable: True
    OU=Domain Controllers:
# end stale_accounts
