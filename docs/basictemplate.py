from pygments.lexer import RegexLexer, include
from pygments.token import Text, Name, String

# Originally written for Python's string.Template
# (https://docs.python.org/3/library/string.html#template-strings)
# But could apply to something processed by envsubst(1), too.

class BasicTemplateLexer(RegexLexer):
    name = 'basictemplate'

    tokens = {
        'root': [
            (r'[^$]+', Text),
            (r'\$\$', String.Escape),
            (r'\$\{', String.Interpol, 'curly'),
            (r'\$[a-zA-Z_]\w*', Name.Variable),
        ],
        'curly': [
            (r'\}', String.Interpol, '#pop'),
            (r'[a-zA-Z_]\w*', Name.Variable),
            include('root'),
        ],
    }
