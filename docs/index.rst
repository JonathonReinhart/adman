ADMan
=====

ADMan is a tool for performing various :doc:`automated tasks <tasks/index>`
against an Active Directory domain.

.. toctree::
   :maxdepth: 1

   installation
   setup
   tasks/index
   configuration
   cli
   troubleshooting
   changelog

Adman can run on any Linux system; the host system does not even need to be
joined to the domain. Adman typically runs with a :doc:`dedicated user <setup>`
(e.g.  ``domain-janitor``) and uses a Kerberos keytab, rather than
password-based authentication.
