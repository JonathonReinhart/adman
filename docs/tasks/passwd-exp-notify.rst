Password expiry notification
============================

ADMan can notify users via email when their password is about to expire in AD.
The notification threshold and interval are configurable, along with the
templated message to be sent.

Actions
-------------------------------------------------
For all users, whose password is not marked as "never expires"
(in ``userAccountControl``), and whose password has ever been set, ADMan will
send an email when their password is about to expire in a given number of days.

.. note:: This requires the user's ``mail`` attribute to be set.

Configuration
-------------------------------------------------

The following configuration options (keys) exist under ``password_expiry_notification``:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``days``
     - *int* or *list<int>*
     - *(required)*
     - A list of the number of days before a user's password expires that they should be notified
   * - ``template_file``
     - :ref:`path<config_type_path>`
     - *(required)*
     - Path to template message to send via email


The template file uses `Python template strings`_ to provide expansion of the
following variables:

.. _Python template strings: https://docs.python.org/3/library/string.html#template-strings

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Variable
     - Description
   * - ``${cn}``
     - User common name (e.g., ``jsmith``)
   * - ``${upn}``
     - User Principal Name (e.g., ``jsmith@example.com``)
   * - ``${expire_days}``
     - The number of days before the user's password expires (with the word "days")
   * - ``${expire_time}``
     - The date/time when the user's password will expire

Example configuration
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_config.yml
   :language: yaml
   :start-at: password_expiry_notification
   :end-before: end password_expiry_notification

Example template
~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_pwnotify.tmpl
   :language: basictemplate


Commands
-------------------------------------------------
Relevant CLI commands:

- :ref:`command_user_checkexpire`
