ID number assignment
====================

Its initial purpose, ADMan can assign :rfc:`2307` LDAP ``uidNumber``/``gidNumber``
attributes for users, computers, and groups.


.. _id_assign_state:

State
-------------------------------------------------
Adman assigns UID/GID numbers sequentially from a user-defined range, and stores
the next-highest value in the ``msSFU30MaxUidNumber``/``msSFU30MaxGidNumber``
attributes in LDAP. This ensures that even if users/groups are removed, UID/GID
values will not be re-used.

These state variables are referred to by ADman as "next uidNumber" and "next gidNumber".


Actions
-------------------------------------------------
For all configured **groups**, ADMan will:

- Assign ``gidNumber`` values

  - The next ``gidNumber`` to be assigned is stored in ``msSFU30MaxGidNumber``.


For all configured **users and computers**, ADMan will:

- Assign ``uidNumber`` values

  - The next ``uidNumber`` to be assigned is stored in ``msSFU30MaxUidNumber``.

- Update the ``gidNumber`` to match that of the user's primary group (``primaryGroupID``)


Configuration
-------------------------------------------------

The following configuration options (keys) exist under ``id_assign``:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``uid_range``
     - :ref:`range<config_type_range>`
     - *(required)*
     - The range of values to use for assigning ``uidNumber`` attributes
   * - ``gid_range``
     - :ref:`range<config_type_range>`
     - *(required)*
     - The range of values to use for assigning ``gidNumber`` attributes
   * - ``computers``
     - *bool*
     - True
     - Whether or not to assign ``uidNumber`` to computer accounts
   * - ``only``
     - :ref:`containers<config_type_containers>`
     - ``'all'``
     - LDAP containers for which members will be assigned IDs


Example configuration
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_config.yml
   :language: yaml
   :start-at: id_assign
   :end-before: end id_assign


Commands
-------------------------------------------------
Relevant CLI commands:

- :ref:`command_assignids`
- :ref:`command_computer_assign`
- :ref:`command_group_assign`
- :ref:`command_user_assign`
