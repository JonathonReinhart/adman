Setup
=====

Account setup
-------------
ADMan requires a privileged domain account (because it does privileged things
in the domain). This account can be named anything, but here we use
``domain-janitor``.

Samba
~~~~~
On a Samba 4 Active Directory domain:

Create the ``domain-janitor`` user and set its password to not expire::

    samba-tool user create domain-janitor --random-password
    samba-tool user setexpiry --noexpiry domain-janitor

Add the user to ``Domain Admins``::

    samba-tool group addmembers 'Domain Admins' domain-janitor

.. todo:: More granular permissions (#17)

Export the user's Kerberos keytab::

    samba-tool domain exportkeytab --principal='domain-janitor' domain-janitor.keytab


.. todo:: Windows (#5)



Configuration
-------------
First, we'll create a minimal :doc:`config file <configuration>` to get up and
going.


- Create ``adman/config.yml`` in the :ref:`appropriate path
  <config_default_path>`:

  .. literalinclude:: minimal-config.yml
     :language: yaml

- Copy the exported keytab to the path specified in ``config.yml``.
  (The above example specifies ``domain-janitor.keytab`` in the same
  directory).

  .. warning::
     ``domain-janitor.keytab`` is password-equivalent; ensure it is
     carefully protected!


First run
---------
To test LDAP connectivity and authentication, run the :ref:`command_user_list`
command::

    adman user list

Before ``uidNumber``/``gidNumber`` values can be assigned, the next-id state
(stored in LDAP) must be initialized using the :ref:`command_state_init`
command::

    adman state init


Run automatically
-----------------

To perform all automated maintenance (assign IDs, UPNs) every minute, run
``crontab -e`` and add this line (changing the path to ``adman`` if
necessary) to run the :ref:`command_allmaint` command::

    */1 * * * * 	/usr/local/bin/adman allmaint


.. note::
   ``adman`` will likely be installed in a path not normally searched by
   ``cron``, so we use the full path (revealed by ``which adman``).

.. note::
   The :ref:`command_allmaint` command does *not* include
   :ref:`command_findstale`, as that will usually be done on a much longer
   interval. Add another cronjob (e.g. weekly) for :ref:`command_findstale`.
