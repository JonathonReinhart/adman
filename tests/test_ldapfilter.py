import pytest
from adman.ldapfilter import *

class TestFilter:
    w = Filter('x=0')
    x = Filter('x=1')
    y = Filter('y=2')
    z = Filter('z=3')

    def test_parens_ok(self):
        assert str(Filter('(j=7)')) == '(j=7)'

    def test_no_parens_ok(self):
        assert str(Filter('j=7')) == '(j=7)'

    def test_mismatched_parens_raise(self):
        with pytest.raises(ValueError):
            Filter('(j=7')

    def test_identity(self):
        assert Filter(self.x) is self.x

    def test_simple(self):
        assert str(self.x) == '(x=1)'


    def test_not(self):
        t = ~self.x
        assert str(t) == '(!(x=1))'

    def test_not_and(self):
        t = ~(self.x & self.y)
        assert str(t) == '(!(&(x=1)(y=2)))'

    def test_not_and(self):
        t = self.x & ~self.y
        assert str(t) == '(&(x=1)(!(y=2)))'


    def test_and2(self):
        t = self.x & self.y
        assert str(t) == '(&(x=1)(y=2))'

    def test_and3(self):
        t = self.x & self.y & self.z
        assert str(t) == '(&(x=1)(y=2)(z=3))'

    def test_and3_group_right(self):
        t = self.x & (self.y & self.z)
        assert str(t) == '(&(x=1)(y=2)(z=3))'

    def test_and4_group_group(self):
        t = (self.w & self.x) & (self.y & self.z)
        assert str(t) == '(&(x=0)(x=1)(y=2)(z=3))'

    def test_and_group_unchanged(self):
        t = self.x & self.y
        dontcare = t & self.z

        # Ensure originals are unmodified
        assert str(self.x) == '(x=1)'
        assert str(self.y) == '(y=2)'
        assert str(self.y) == '(z=3)'



    def test_or2(self):
        t = self.x | self.y
        assert str(t) == '(|(x=1)(y=2))'

    def test_or3(self):
        t = self.x | self.y | self.z
        assert str(t) == '(|(x=1)(y=2)(z=3))'

    def test_or3_group_right(self):
        t = self.x | (self.y | self.z)
        assert str(t) == '(|(x=1)(y=2)(z=3))'

    def test_or4_group_group(self):
        t = (self.w | self.x) | (self.y | self.z)
        assert str(t) == '(|(x=0)(x=1)(y=2)(z=3))'

    def test_and_group_unchanged(self):
        t = self.x | self.y
        dontcare = t | self.z

        # Ensure originals are unmodified
        assert str(self.x) == '(x=1)'
        assert str(self.y) == '(y=2)'
        assert str(t) == '(|(x=1)(y=2))'


    def test_or2_and(self):
        t = (self.x | self.y) & self.z
        assert str(t) == '(&(|(x=1)(y=2))(z=3))'

    def test_or_and2(self):
        t = self.x | (self.y & self.z)
        assert str(t) == '(|(x=1)(&(y=2)(z=3)))'

    def test_and2_or(self):
        t = (self.x & self.y) | self.z
        assert str(t) == '(|(&(x=1)(y=2))(z=3))'

    def test_and_or2(self):
        t = self.x & (self.y | self.z)
        assert str(t) == '(&(x=1)(|(y=2)(z=3)))'
