from textwrap import dedent as D
from datetime import datetime, timedelta

from adman.kerberos import parse_klist_output

class Test_Kerberos:
    def test_heimdal(self):
        s = D("""\
            Credentials cache: FILE:test.cc
                    Principal: joe@AD-TEST.VX

              Issued                Expires               Principal
            Dec 30 12:05:58 2019  Dec 30 22:05:58 2019  krbtgt/AD-TEST.VX@AD-TEST.VX
            """)
        cc = parse_klist_output(s)

        assert len(cc.tickets) == 1

        t = cc.tickets['krbtgt/AD-TEST.VX@AD-TEST.VX']
        assert t.issued == datetime(2019, 12, 30, 12, 5, 58)
        assert t.expires == datetime(2019, 12, 30, 22, 5, 58)

    def test_heimdal_expired(self):
        s = D("""\
            Credentials cache: FILE:test.cc
                    Principal: joe@AD-TEST.VX

              Issued                Expires        Principal
            Dec 30 12:05:58 2019  >>>Expired<<<  krbtgt/AD-TEST.VX@AD-TEST.VX
            """)
        cc = parse_klist_output(s)

        assert len(cc.tickets) == 1

        t = cc.tickets['krbtgt/AD-TEST.VX@AD-TEST.VX']
        assert t.issued == datetime(2019, 12, 30, 12, 5, 58)
        assert t.expires == None
        assert t.remaining == timedelta(0)

    def test_mit(self):
        s = D("""\
            Ticket cache: FILE:/tmp/domain-janitor.cc
            Default principal: domain-janitor@AD-TEST.VX
            
            Valid starting     Expires            Service principal
            01/28/21 22:34:09  01/29/21 08:34:09  krbtgt/AD-TEST.VX@AD-TEST.VX
            	renew until 01/29/21 22:34:09
            01/28/21 22:34:09  01/29/21 08:34:09  ldap/dc1.ad-test.vx@
            	renew until 01/29/21 22:34:09
            01/28/21 22:34:09  01/29/21 08:34:09  ldap/dc1.ad-test.vx@AD-TEST.VX
            	renew until 01/29/21 22:34:09
            """)
        cc = parse_klist_output(s)

        assert len(cc.tickets) == 3

        issued_time = datetime(2021, 1, 28, 22, 34, 9)
        expire_time = datetime(2021, 1, 29, 8, 34, 9)

        t = cc.tickets['krbtgt/AD-TEST.VX@AD-TEST.VX']
        assert t.issued == issued_time
        assert t.expires == expire_time

        t = cc.tickets['ldap/dc1.ad-test.vx@']
        assert t.issued == issued_time
        assert t.expires == expire_time

        t = cc.tickets['ldap/dc1.ad-test.vx@AD-TEST.VX']
        assert t.issued == issued_time
        assert t.expires == expire_time

    def test_mit_v1_18(self):
        # v1.18 might add a line: "Ticket server: "
        # https://github.com/krb5/krb5/commit/f174919a600ab617a881500e3ead98ba9f49c62e
        s = D("""\
            Ticket cache: FILE:/tmp/domain-janitor.cc
            Default principal: domain-janitor@AD-TEST.VX
            
            Valid starting     Expires            Service principal
            10/22/21 23:12:05  10/23/21 09:12:05  krbtgt/AD-TEST.VX@AD-TEST.VX
            	renew until 10/23/21 23:12:05
            10/22/21 23:12:05  10/23/21 09:12:05  ldap/dc1.ad-test.vx@
            	renew until 10/23/21 23:12:05
            	Ticket server: ldap/dc1.ad-test.vx@AD-TEST.VX
            10/22/21 23:12:05  10/23/21 09:12:05  cifs/dc1.ad-test.vx@AD-TEST.VX
        """)

        cc = parse_klist_output(s)

        assert len(cc.tickets) == 3

        issued_time = datetime(2021, 10, 22, 23, 12, 5)
        expire_time = datetime(2021, 10, 23, 9, 12, 5)

        t = cc.tickets['krbtgt/AD-TEST.VX@AD-TEST.VX']
        assert t.issued == issued_time
        assert t.expires == expire_time

        t = cc.tickets['ldap/dc1.ad-test.vx@']
        assert t.issued == issued_time
        assert t.expires == expire_time

        t = cc.tickets['cifs/dc1.ad-test.vx@AD-TEST.VX']
        assert t.issued == issued_time
        assert t.expires == expire_time
