import pytest
from adman.smbacl import *

class TestSMBACL:
    def test_libsmb_xattr_values(self):
        # These values come from source3/libsmb/libsmb_xattr.c (parse_ace).
        # Make sure we understand the enum names.
        lookup = {
            "R": 0x00120089,
            "W": 0x00120116,
            "X": 0x001200a0,
            "D": 0x00010000,
            "P": 0x00040000,
            "O": 0x00080000,
            "READ":   0x001200a9,
            "CHANGE": 0x001301bf,
            "FULL":   0x001f01ff,
        }

        assert lookup['R'] == FileAceAccess.SEC_RIGHTS_FILE_READ
        assert lookup['W'] == FileAceAccess.SEC_RIGHTS_FILE_WRITE
        assert lookup['X'] == FileAceAccess.SEC_RIGHTS_FILE_EXECUTE
        assert lookup['D'] == FileAceAccess.SEC_STD_DELETE
        assert lookup['P'] == FileAceAccess.SEC_STD_WRITE_DAC
        assert lookup['O'] == FileAceAccess.SEC_STD_WRITE_OWNER

        assert lookup['READ'] == (
                  FileAceAccess.SEC_RIGHTS_FILE_READ
                | FileAceAccess.SEC_RIGHTS_FILE_EXECUTE)

        assert lookup['CHANGE'] == (
                  FileAceAccess.SEC_RIGHTS_FILE_READ
                | FileAceAccess.SEC_RIGHTS_FILE_WRITE
                | FileAceAccess.SEC_RIGHTS_FILE_EXECUTE
                | FileAceAccess.SEC_STD_DELETE)

        assert lookup['FULL'] == FileAceAccess.SEC_RIGHTS_FILE_ALL

    sample_ace_str = 'NT AUTHORITY\\SYSTEM:0/3/0x001f01ff'

    def test_parse_ace_val(self):
        SmbAce.parse(self.sample_ace_str)

    @pytest.mark.xfail(reason="See note on SmbAceVal.__str__")
    def test_ace_str_roundtrip(self):
        ace = SmbAce.parse(self.sample_ace_str)
        assert str(ace) == self.sample_ace_str

    def test_ace_value_attrs(self):
        ace = SmbAce.parse(self.sample_ace_str)
        assert ace.value.type == AceType.ACCESS_ALLOWED
        assert ace.value.flags == AceFlag.OBJECT_INHERIT | AceFlag.CONTAINER_INHERIT
        assert ace.value.mask == FileAceAccess.SEC_RIGHTS_FILE_ALL


class Test_SmbSecurityDesc:
    def test_parse(self):
        raw = r'REVISION:1,OWNER:ADTEST\fileshare-owner,GROUP:ADTEST\Domain Users,ACL:\Everyone:0/3/0x001200a9,ACL:\CREATOR OWNER:0/11/0x001f01ff,ACL:\CREATOR GROUP:0/11/0x001200a9,ACL:BUILTIN\Administrators:0/0/0x001f01ff,ACL:ADTEST\Domain Users:0/0/0x001200a9,ACL:ADTEST\bob:0/3/0x001f01ff'
        desc = SmbSecurityDesc.parse(raw)
        print(repr(desc))
