#!/bin/sh
exec python3 -m pytest -v --log-level=DEBUG \
    --cov=adman --cov=tests --cov-branch \
    --cov-report=html \
    --cov-report=xml:coverage.xml \
    --junit-xml=report.xml \
    "$@"
